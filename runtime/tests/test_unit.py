import numpy as np

from SkMnist import SkMnist


def test_predictor():
    predictor = SkMnist()
    prediction = predictor.predict([[0] * 784], None)
    assert isinstance(prediction, np.ndarray)
    assert prediction.shape == (1, 10)
